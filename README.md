# MIP Minecraft 1.12.2 modpack

Данный модпак предназначен для MIP Minecraft сервера с поддержкой модов версии 1.12.2

## Установка

```bash
git clone https://YSelyuntsev@bitbucket.org/YSelyuntsev/minecraft-1.12-mods.git ~/minecraft-1.12-mods
```

```bash
cd ~/minecraft-1.12-mods
```

```bash
./install.sh
```

```bash
rm -r ~/minecraft-1.12-mods
```

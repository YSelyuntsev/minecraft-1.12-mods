#/bin/sh
GREEN='\033[1;32m'
PURPLE='\033[1;35m'
YELLOW='\033[1;33m'
NC='\033[0m' # No Color

echo -e "${GREEN}=====${NC} ${PURPLE}MIP MINECRAFT 1.12 MODS INSTALLER${NC} ${GREEN}=====${NC}"
unzip mods.zip -d ~/.minecraft/mods
echo -e "${GREEN}=====${NC} ${YELLOW}DONE!${NC} ${GREEN}=====${NC}"
